﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gpy_common
{
    public class Global
    {

        private SqLiteHelper _sqlLiteHelper;

        private static string _lockFlag = "GlobalLock";

        private static Global _instance;

        //mqtt是否已经连接
        public bool isMqttClientConnected = false;

        private Global()
        {

        }

        public static Global Instance
        {
            get
            {
                lock (_lockFlag)
                {
                    if (_instance == null)
                    {
                        _instance = new Global();
                    }
                    return _instance;
                }
            }
        }

        public SqLiteHelper sqlLiteHelper
        {
            get { return _sqlLiteHelper; }
            set { _sqlLiteHelper = value; }
        }


        public void InitPositionSqLite()
        //SQLiteLog.LogMessage("Email:" + reader.GetString(reader.GetOrdinal("Email")));

        {
            _sqlLiteHelper = new SqLiteHelper("Data Source=D:/mydb.db;Version=3;Connect Timeout=30;Pooling=true;Journal Mode=WAL");
            //创建名为positions的数据表
            //_sqlLiteHelper.CreateTable("positions", new string[] { "timestamp", "data" }, new string[] { "TEXT", "TEXT" });
        }
    }
}
