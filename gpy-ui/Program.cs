﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using gpy_common;
using gpy_models;

namespace gpy_ui
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //顺序勿动
            //初始化positions数据库
            Global.Instance.InitPositionSqLite();

           Application.Run(new Main());
     
     }
    }
}
