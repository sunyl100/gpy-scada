﻿using gpy_common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gpy_ui.main
{
    public partial class base_bu_add : Form
    {
        public Main main ;
        public base_bu_add()
        {
            InitializeComponent();
        }
        public base_bu_add(Main main)
        {
            this.main = main;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //TODO 接受信息
            //查询化学最新记录
            //回填信息 update
            //回显页面
            var cj = textBox1.Text;
            var lh = textBox2.Text;
            var lch = textBox3.Text;
            var ph = textBox4.Text;
            var qh_if = textBox5.Text;
            var czy = textBox6.Text;
            SQLiteDataReader reader1 = Global.Instance.sqlLiteHelper.ExecuteQuery("select *  FROM data_record " +
                "order by sj desc limit 1" );
            while (reader1.Read())
            {
                if (reader1.IsDBNull(reader1.GetOrdinal("ph")))
                {
                    int id = reader1.GetInt32(reader1.GetOrdinal("id"));
                    var values = new string[] { cj, lh, lch, ph, qh_if, czy };
                    var clumns = new string[] { "cj", "lh", "lch", "ph", "qh_if", "czy" };
                    Global.Instance.sqlLiteHelper.UpdateValues("data_record", clumns, values, "id", id+"", "=");
                    MessageBox.Show("光谱基础数据已更新！");
                    this.Hide();
                    this.main.panal_child(cj,lh,lch,ph,qh_if,czy); 

                }
                else {
                    var ph_old =  reader1.GetString(reader1.GetOrdinal("ph"));
                    MessageBox.Show("已存在基础信息，无需补录！");
                    //this.main.panal_child(cj, lh, lch, ph, qh_if, czy);

                }
            }





        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
