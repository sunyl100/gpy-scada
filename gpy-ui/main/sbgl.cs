﻿using gpy_common;
using gpy_ui.xtgl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gpy_ui
{
    public partial class Form3_sbgl : Form
    {
        public equipment equipment;
        public Form3_sbgl()
        {
            InitializeComponent();
        }
        public Form3_sbgl(equipment equipment)
        {
            InitializeComponent();
            this.equipment = equipment;
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form3_sbgl_Load(object sender, EventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var factory = textBox2.Text;
            var model = textBox3.Text;
            var protocol = comboBox1.Text;
            var storage_type = comboBox2.Text;
            var storage_url = textBox6.Text;
            var ip = textBox7.Text;
            var port = textBox5.Text;
            var values = new string[] { name, factory, model, protocol, storage_type, storage_url, ip,port };
            var clumns = new string[] { "name", "factory", "model", "protocol", "storage_type", "storage_url" , "ip","port" };
            if (!Valiad.ifnull(values))
            {
                MessageBox.Show("所有数据均为必填项！");
                return;
            }
            //插入数据
            Global.Instance.sqlLiteHelper.InsertValuesAutoId("sys_equipment", clumns, values);
            Console.WriteLine("写入 sys_equipment" + values.ToString());
            MessageBox.Show("保存成功！");
            this.Hide();
            equipment.search();
          ;
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {

        }
    }
}
