﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//23.11.03 20:16添加
using System.Data.SQLite;
using gpy_common;
using gpy_ui.main;
using gpy_ui.xtgl;
using System.IO.Ports;

namespace gpy_ui
{
    public partial class Main : Form
    {
        private SerialPort my_serialPort;

        public Main()
        {
            InitializeComponent();
            listBox1.Items.Add("08:00:04 合金配料数据接收成功。");
            listBox1.Items.Add("08:00:02 炉次号A120光谱数据发送成功!");
            listBox1.Items.Add("08:00:00 合金配料数据接收成功。");
            listBox1.Items.Add("08:00:01 炉次号A120光谱数据发送成功!");

            search_panel3();
            search_table();
            serialPort_initialization();//进入界面连接串口


        }

        public void serialPort_initialization()
        {
            my_serialPort = new SerialPort();
            my_serialPort.PortName = "COM5";
            my_serialPort.BaudRate = 9600; //波特率  
            my_serialPort.DataBits = 8; //数据位
            my_serialPort.Parity = Parity.None; //奇偶校验  
            my_serialPort.StopBits = StopBits.One; //停止位
            //my_serialPort.Encoding = Encoding.Default;
            //my_serialPort.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort1_Rcv);
            my_serialPort.Open();
            if (my_serialPort.IsOpen)
            {
                Console.WriteLine("串口已连接");
            }
            else {
                Console.WriteLine("串口未连接");
            }

        }

        ///串口接收方法
        //void serialPort1_Rcv(object sender, SerialDataReceivedEventArgs e)
        //{
        //    UInt16 bufflen = (UInt16)my_serialPort.BytesToRead;  //返回接收到的数据个数
        //    byte[] dat = new byte[bufflen];
        //    my_serialPort.Read(dat, 0, bufflen);
        //    String res = Encoding.ASCII.GetString(dat);
        //    string[] a = { res };
        //    //SQLiteDataReader reader2 = Global.Instance.sqlLiteHelper.InsertValues("data_record", a);
        //}
        public void search_panel3()
        {
            SQLiteDataReader reader1 = Global.Instance.sqlLiteHelper.ExecuteQuery("select *  FROM data_record order by sj desc limit 1 ");
            while (reader1.Read())
            {
                if (!reader1.IsDBNull(reader1.GetOrdinal("ph")))
                {
                    var ph = reader1.GetString(reader1.GetOrdinal("ph"));
                    SQLiteDataReader bz = Global.Instance.sqlLiteHelper.ExecuteQuery("select *  FROM Spectral_standard_value  where  ph =  '" + ph + "'");
                    while (bz.Read())
                    {
                        //TODO  1 赋值挨个  == 标准
                        if (!bz.IsDBNull(bz.GetOrdinal("C_l")))
                        {
                            var C_l = bz.GetString(bz.GetOrdinal("C_l"));
                            label301.Text = C_l;

                        }
                        if (!bz.IsDBNull(bz.GetOrdinal("C_h")))
                        {
                            label302.Text = bz.GetString(bz.GetOrdinal("C_h"));
                        }
                        if (!bz.IsDBNull(bz.GetOrdinal("Si_l")))
                        {
                            label303.Text = bz.GetString(bz.GetOrdinal("Si_l"));
                        }
                        if (!bz.IsDBNull(bz.GetOrdinal("Si_h")))
                        {
                            label304.Text = bz.GetString(bz.GetOrdinal("Si_h"));
                        }
                        if (!bz.IsDBNull(bz.GetOrdinal("Mn_l")))
                        {
                            label305.Text = bz.GetString(bz.GetOrdinal("Mn_l"));
                        }
                        if (!bz.IsDBNull(bz.GetOrdinal("Mn_h")))
                        {
                            label306.Text = bz.GetString(bz.GetOrdinal("Mn_h"));
                        }
                        if (!bz.IsDBNull(bz.GetOrdinal("S_l")))
                        {
                            label307.Text = bz.GetString(bz.GetOrdinal("S_l"));
                        }
                        if (!bz.IsDBNull(bz.GetOrdinal("S_h")))
                        {
                            label308.Text = bz.GetString(bz.GetOrdinal("S_h"));
                        }
                        if (!bz.IsDBNull(bz.GetOrdinal("P_l")))
                        {
                            label309.Text = bz.GetString(bz.GetOrdinal("P_l"));
                        }
                        if (!bz.IsDBNull(bz.GetOrdinal("P_h")))
                        {
                            label310.Text = bz.GetString(bz.GetOrdinal("P_h"));
                        }
                        if (!bz.IsDBNull(bz.GetOrdinal("CEQ_l")))
                        {
                            label311.Text = bz.GetString(bz.GetOrdinal("CEQ_l"));
                        }
                        if (!bz.IsDBNull(bz.GetOrdinal("CEQ_h")))
                        {
                            label312.Text = bz.GetString(bz.GetOrdinal("CEQ_h"));
                        }
                        if (!bz.IsDBNull(bz.GetOrdinal("Mg_l")))
                        {
                            label313.Text = bz.GetString(bz.GetOrdinal("Mg_l"));
                        }
                        if (!bz.IsDBNull(bz.GetOrdinal("Mg_h")))
                        {
                            label314.Text = bz.GetString(bz.GetOrdinal("Mg_h"));
                        }
                        if (!bz.IsDBNull(bz.GetOrdinal("Cr_l")))
                        {
                            label315.Text = bz.GetString(bz.GetOrdinal("Cr_l"));
                        }
                        if (!bz.IsDBNull(bz.GetOrdinal("Cr_h")))
                        {
                            label316.Text = bz.GetString(bz.GetOrdinal("Cr_h"));
                        }
                        if (!bz.IsDBNull(bz.GetOrdinal("Ni_l")))
                        {
                            label317.Text = bz.GetString(bz.GetOrdinal("Ni_l"));
                        }
                        if (!bz.IsDBNull(bz.GetOrdinal("Ni_h")))
                        {
                            label318.Text = bz.GetString(bz.GetOrdinal("Ni_h"));
                        }
                        if (!bz.IsDBNull(bz.GetOrdinal("Cu_l")))
                        {
                            label319.Text = bz.GetString(bz.GetOrdinal("Cu_l"));
                        }
                        if (!bz.IsDBNull(bz.GetOrdinal("Cu_h")))
                        {
                            label320.Text = bz.GetString(bz.GetOrdinal("Cu_h"));
                        }
                        if (!bz.IsDBNull(bz.GetOrdinal("Mo_l")))
                        {
                            label321.Text = bz.GetString(bz.GetOrdinal("Mo_l"));
                        }
                        if (!bz.IsDBNull(bz.GetOrdinal("Mo_h")))
                        {
                            label322.Text = bz.GetString(bz.GetOrdinal("Mo_h"));
                        }
                        if (!bz.IsDBNull(bz.GetOrdinal("Al_l")))
                        {
                            label323.Text = bz.GetString(bz.GetOrdinal("Al_l"));
                        }
                        if (!bz.IsDBNull(bz.GetOrdinal("Al_h")))
                        {
                            label324.Text = bz.GetString(bz.GetOrdinal("Al_h"));
                        }
                        if (!bz.IsDBNull(bz.GetOrdinal("Ti_l")))
                        {
                            label325.Text = bz.GetString(bz.GetOrdinal("Ti_l"));
                        }
                        if (!bz.IsDBNull(bz.GetOrdinal("Ti_h")))
                        {
                            label326.Text = bz.GetString(bz.GetOrdinal("Ti_h"));
                        }
                        if (!bz.IsDBNull(bz.GetOrdinal("V_l")))
                        {
                            label327.Text = bz.GetString(bz.GetOrdinal("V_l"));
                        }
                        if (!bz.IsDBNull(bz.GetOrdinal("V_h")))
                        {
                            label328.Text = bz.GetString(bz.GetOrdinal("V_h"));
                        }


                    }

                }

                //有没有牌号都进行最新的检测记录的赋值
                //赋值基础数据
                if (!reader1.IsDBNull(reader1.GetOrdinal("cj")))
                {
                    comboBox2.Text = reader1.GetString(reader1.GetOrdinal("cj"));
                }
                if (!reader1.IsDBNull(reader1.GetOrdinal("lh")))
                {
                    comboBox3.Text = reader1.GetString(reader1.GetOrdinal("lh"));
                }
                if (!reader1.IsDBNull(reader1.GetOrdinal("lch")))
                {
                    textBox3.Text = reader1.GetString(reader1.GetOrdinal("lch"));
                }
                if (!reader1.IsDBNull(reader1.GetOrdinal("ph")))
                {
                    comboBox4.Text = reader1.GetString(reader1.GetOrdinal("ph"));
                }
                if (!reader1.IsDBNull(reader1.GetOrdinal("qh_if")))
                {
                    comboBox1.Text = reader1.GetString(reader1.GetOrdinal("qh_if"));
                }
                if (!reader1.IsDBNull(reader1.GetOrdinal("czy")))
                {
                    comboBox5.Text = reader1.GetString(reader1.GetOrdinal("czy"));
                }
                // 2 TODO 赋值挨个  == 赋值辅料 右上角

                if (!reader1.IsDBNull(reader1.GetOrdinal("ztj_C")))
                {
                    label46.Text = reader1.GetString(reader1.GetOrdinal("ztj_C"));
                }

                if (!reader1.IsDBNull(reader1.GetOrdinal("mt_Mn")))
                {
                    label45.Text = reader1.GetString(reader1.GetOrdinal("mt_Mn"));
                }

                if (!reader1.IsDBNull(reader1.GetOrdinal("yyg_Si")))
                {
                    label44.Text = reader1.GetString(reader1.GetOrdinal("yyg_Si"));
                }

                if (!reader1.IsDBNull(reader1.GetOrdinal("weight_TY")))
                {
                    label47.Text = reader1.GetString(reader1.GetOrdinal("weight_TY"));
                }
                // 3 TODO 赋值挨个  == 实际值
                if (!reader1.IsDBNull(reader1.GetOrdinal("C")))
                {
                    label201.Text = reader1.GetString(reader1.GetOrdinal("C"));
                }

                if (!reader1.IsDBNull(reader1.GetOrdinal("Si")))
                {
                    label202.Text = reader1.GetString(reader1.GetOrdinal("Si"));
                }

                if (!reader1.IsDBNull(reader1.GetOrdinal("Mn")))
                {
                    label203.Text = reader1.GetString(reader1.GetOrdinal("Mn"));
                }

                if (!reader1.IsDBNull(reader1.GetOrdinal("S")))
                {
                    label204.Text = reader1.GetString(reader1.GetOrdinal("S"));
                }

                if (!reader1.IsDBNull(reader1.GetOrdinal("P")))
                {
                    label205.Text = reader1.GetString(reader1.GetOrdinal("P"));
                }

                if (!reader1.IsDBNull(reader1.GetOrdinal("CEQ")))
                {
                    label206.Text = reader1.GetString(reader1.GetOrdinal("CEQ"));
                }

                if (!reader1.IsDBNull(reader1.GetOrdinal("Mg")))
                {
                    label207.Text = reader1.GetString(reader1.GetOrdinal("Mg"));
                }

                if (!reader1.IsDBNull(reader1.GetOrdinal("Cr")))
                {
                    label208.Text = reader1.GetString(reader1.GetOrdinal("Cr"));
                }

                if (!reader1.IsDBNull(reader1.GetOrdinal("Ni")))
                {
                    label209.Text = reader1.GetString(reader1.GetOrdinal("Ni"));
                }

                if (!reader1.IsDBNull(reader1.GetOrdinal("Cu")))
                {
                    label210.Text = reader1.GetString(reader1.GetOrdinal("Cu"));
                }

                if (!reader1.IsDBNull(reader1.GetOrdinal("Mo")))
                {
                    label211.Text = reader1.GetString(reader1.GetOrdinal("Mo"));
                }

                if (!reader1.IsDBNull(reader1.GetOrdinal("Al")))
                {
                    label212.Text = reader1.GetString(reader1.GetOrdinal("Al"));
                }

                if (!reader1.IsDBNull(reader1.GetOrdinal("Ti")))
                {
                    label213.Text = reader1.GetString(reader1.GetOrdinal("Ti"));
                }

                if (!reader1.IsDBNull(reader1.GetOrdinal("V")))
                {
                    label214.Text = reader1.GetString(reader1.GetOrdinal("V"));
                }

                if (!reader1.IsDBNull(reader1.GetOrdinal("sj")))
                {
                    label37.Text = reader1.GetString(reader1.GetOrdinal("sj"));
                }

            }



        }
        public void search_table()
        {
            string ksTime = dateTimePicker1.Value.ToString("yyyy-MM-dd");
            string endTime = dateTimePicker2.Value.ToString("yyyy-MM-dd");
            var connect = Global.Instance.sqlLiteHelper.sqliteConnection;
            var Datatable = new DataTable();
            var adp = new SQLiteDataAdapter("select * from data_record where sj between '" + ksTime + "'+' 00:00:00' and '" + endTime + "'+' 24:00:00'", connect);
            adp.Fill(Datatable);
            dataGridView1.DataSource = Datatable;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
            //var connect = Global.Instance.sqlLiteHelper.sQLiteConnection;
            //var Datatable = new DataTable();
            //var adp = new SQLiteDataAdapter("select * from data_record", connect);
            //adp.Fill(Datatable);
            //dataGridView1.DataSource = Datatable;
        }

        private void label38_Click(object sender, EventArgs e)
        {

        }

        private void label32_Click(object sender, EventArgs e)
        {
            Form Form1 = new Main();
            Form1.ShowDialog(this);
            this.Close();
        }

        private void label33_Click(object sender, EventArgs e)
        {
            //打开配料管理界面
            Form fm = new mixture();
            fm.ShowDialog();
        }

        private void label34_Click(object sender, EventArgs e)
        {
            //打开设备管理界面
            Form fm = new equipment();
            fm.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            search_table();
        }


        private void dataGridView1_CellContentClick_2(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private static byte[] strToToHexByte(string hexString) 
        {
            hexString = hexString.Replace(" ","");
            if ((hexString.Length % 2) != 0)
                hexString += " ";
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i=0;i<returnBytes.Length; i++)
            returnBytes[i] = Convert.ToByte(hexString.Substring(i* 2,2),16);
            return returnBytes;
        }
        private void button2_Click_1(object sender, EventArgs e)
        {
            //my_serialPort.Write("fe011000000028005053690000026903a04d6e0000010502e30053000000af01880070000001cd03ab4d670000004b0124416c000001e103bf4e690000005500e854690000011702324365000002ad03b84d6f0000017b02d2ff");
            //string a = "fe011000000028005053690000026903a04d6e0000010502e30053000000af01880070000001cd03ab4d670000004b0124416c000001e103bf4e690000005500e854690000011702324365000002ad03b84d6f0000017b02d2ff";

            //strToToHexByte(a);
            // 准备要发送的16进制数据  
            byte[] dataToSend = { 0xfe, 0x01, 0x10, 0x00, 0x00, 0x00, 0x04, 0x00, 0x08, 0x4d, 0x6f, 0x00, 0x00, 0x0b, 0xab, 0x16, 0x96, 0xff }; // 用你自己的数据替换这个数组  
                
            // 发送数据  
            my_serialPort.Write(dataToSend, 0, dataToSend.Length);


            //if (my_serialPort.IsOpen)
            //{
            //    try
            //    {
            //        byte[] sendData = null;
            //        string send = a.Trim();
            //        var showText = "16进制(HEX)";
            //        //按照指定编码将string编程字节数组
            //        byte[] b = Encoding.GetEncoding("GBK").GetBytes(send);
            //        if (showText == "16进制(HEX)")
            //        {
            //            string result = string.Empty;
            //            //逐字节变为16进制字符
            //            for (int i = 0; i < b.Length; i++)
            //            {
            //                result += Convert.ToString(b[i], 16).ToUpper() + " ";
            //            }
            //            sendData = Encoding.GetEncoding("GBK").GetBytes(result);
            //        }
            //        else
            //        {
            //            sendData = b;
            //        }
            //        my_serialPort.Write(sendData, 0, sendData.Length);
            //    }
            //    catch (Exception ex)
            //    {

            //        MessageBox.Show("发送失败：" + ex.Message, "Error");
            //    }
            //}
            //else
            //{
            //    MessageBox.Show("请先打开串口", "Error");
            //}


        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //每秒进行实际值与上下限比较，并用不同颜色显示不同的情况
            //sjz 实际值；bzz_l标准值下限；bzz_h标准值上限
            for (int i = 201; i < 215; i++)
            {
                double sjz = Convert.ToDouble(((Label)(this.Controls.Find("label" + i.ToString(), true)[0])).Text);//获取实际值的文本数值
                double bzz_l = Convert.ToDouble(((Label)(this.Controls.Find("label" + (2 * (i - 200) - 1 + 300).ToString(), true)[0])).Text);//获取标准值下限的文本数值
                double bzz_h = Convert.ToDouble(((Label)(this.Controls.Find("label" + (2 * (i - 200) + 300).ToString(), true)[0])).Text);//获取标准值上限的文本数值
                if (sjz > bzz_l && sjz < bzz_h)
                {
                    ((Label)(this.Controls.Find("label" + i.ToString(), true)[0])).ForeColor = Color.Green;
                }
                else
                {
                    ((Label)(this.Controls.Find("label" + i.ToString(), true)[0])).ForeColor = Color.Red;
                }
            }
        }

        private void label301_Click(object sender, EventArgs e)
        {
        


        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label53_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            new base_bu_add(this).ShowDialog();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        public void panal_child(String cj,String lh,String lch,String ph,String qh_if,String czy) {
            Console.WriteLine(111);
            Console.WriteLine(textBox3);
            comboBox2.Text = cj;
            comboBox3.Text = lh;
            textBox3.Text = lch;
            comboBox4.Text = lch;
            comboBox4.Text = ph;
            comboBox1.Text = qh_if;
            comboBox5.Text = czy;
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label201_Click(object sender, EventArgs e)
        {

        }

        private void label202_Click(object sender, EventArgs e)
        {

        }

        private void label35_Click(object sender, EventArgs e)
        {
            new led().ShowDialog();
                 
        }

        private void label37_Click(object sender, EventArgs e)
        {

        }
    }
}
