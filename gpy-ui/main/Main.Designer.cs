﻿
namespace gpy_ui
{
    partial class Main
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.panel1 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label37 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label328 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label327 = new System.Windows.Forms.Label();
            this.label214 = new System.Windows.Forms.Label();
            this.label326 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label325 = new System.Windows.Forms.Label();
            this.label213 = new System.Windows.Forms.Label();
            this.label324 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label323 = new System.Windows.Forms.Label();
            this.label212 = new System.Windows.Forms.Label();
            this.label322 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label321 = new System.Windows.Forms.Label();
            this.label211 = new System.Windows.Forms.Label();
            this.label320 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label319 = new System.Windows.Forms.Label();
            this.label210 = new System.Windows.Forms.Label();
            this.label318 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label317 = new System.Windows.Forms.Label();
            this.label209 = new System.Windows.Forms.Label();
            this.label316 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label315 = new System.Windows.Forms.Label();
            this.label208 = new System.Windows.Forms.Label();
            this.label314 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label313 = new System.Windows.Forms.Label();
            this.label207 = new System.Windows.Forms.Label();
            this.label312 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label311 = new System.Windows.Forms.Label();
            this.label206 = new System.Windows.Forms.Label();
            this.label310 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label309 = new System.Windows.Forms.Label();
            this.label205 = new System.Windows.Forms.Label();
            this.label308 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label307 = new System.Windows.Forms.Label();
            this.label204 = new System.Windows.Forms.Label();
            this.label306 = new System.Windows.Forms.Label();
            this.label304 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label305 = new System.Windows.Forms.Label();
            this.label303 = new System.Windows.Forms.Label();
            this.label203 = new System.Windows.Forms.Label();
            this.label202 = new System.Windows.Forms.Label();
            this.label302 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label301 = new System.Windows.Forms.Label();
            this.label201 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.序号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.车间 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.炉次 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.炉次号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.是否球化 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.操作员 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.时间 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.C = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Si = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.S = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.P = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CEQ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ni = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Al = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ti = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.V = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ce = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.button1 = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.comboBox5);
            this.panel1.Controls.Add(this.comboBox4);
            this.panel1.Controls.Add(this.comboBox3);
            this.panel1.Controls.Add(this.comboBox2);
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.textBox3);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(37, 40);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(512, 187);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button3.Location = new System.Drawing.Point(416, 14);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(89, 24);
            this.button3.TabIndex = 15;
            this.button3.Text = "补录基础信息";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // comboBox5
            // 
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Items.AddRange(new object[] {
            "球化前",
            "球化后"});
            this.comboBox5.Location = new System.Drawing.Point(340, 147);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(100, 20);
            this.comboBox5.TabIndex = 14;
            this.comboBox5.SelectedIndexChanged += new System.EventHandler(this.comboBox5_SelectedIndexChanged);
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Items.AddRange(new object[] {
            "QT-450"});
            this.comboBox4.Location = new System.Drawing.Point(340, 95);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(100, 20);
            this.comboBox4.TabIndex = 13;
            this.comboBox4.SelectedIndexChanged += new System.EventHandler(this.comboBox4_SelectedIndexChanged);
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "球化前",
            "球化后"});
            this.comboBox3.Location = new System.Drawing.Point(340, 44);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(100, 20);
            this.comboBox3.TabIndex = 12;
            this.comboBox3.SelectedIndexChanged += new System.EventHandler(this.comboBox3_SelectedIndexChanged);
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "球化前",
            "球化后"});
            this.comboBox2.Location = new System.Drawing.Point(145, 44);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(100, 20);
            this.comboBox2.TabIndex = 11;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "球化前",
            "球化后"});
            this.comboBox1.Location = new System.Drawing.Point(145, 147);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(100, 20);
            this.comboBox1.TabIndex = 1;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(283, 150);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 9;
            this.label7.Text = "检测员";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(78, 150);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 12);
            this.label6.TabIndex = 9;
            this.label6.Text = "球化前/后";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(283, 98);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 12);
            this.label5.TabIndex = 7;
            this.label5.Text = "牌号";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(145, 95);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 21);
            this.textBox3.TabIndex = 6;
            this.textBox3.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(78, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 5;
            this.label4.Text = "炉次号";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(283, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "炉号";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(78, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "车间";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "基础数据";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label37);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.label328);
            this.panel2.Controls.Add(this.label103);
            this.panel2.Controls.Add(this.label327);
            this.panel2.Controls.Add(this.label214);
            this.panel2.Controls.Add(this.label326);
            this.panel2.Controls.Add(this.label99);
            this.panel2.Controls.Add(this.label325);
            this.panel2.Controls.Add(this.label213);
            this.panel2.Controls.Add(this.label324);
            this.panel2.Controls.Add(this.label95);
            this.panel2.Controls.Add(this.label323);
            this.panel2.Controls.Add(this.label212);
            this.panel2.Controls.Add(this.label322);
            this.panel2.Controls.Add(this.label91);
            this.panel2.Controls.Add(this.label321);
            this.panel2.Controls.Add(this.label211);
            this.panel2.Controls.Add(this.label320);
            this.panel2.Controls.Add(this.label87);
            this.panel2.Controls.Add(this.label319);
            this.panel2.Controls.Add(this.label210);
            this.panel2.Controls.Add(this.label318);
            this.panel2.Controls.Add(this.label83);
            this.panel2.Controls.Add(this.label317);
            this.panel2.Controls.Add(this.label209);
            this.panel2.Controls.Add(this.label316);
            this.panel2.Controls.Add(this.label79);
            this.panel2.Controls.Add(this.label315);
            this.panel2.Controls.Add(this.label208);
            this.panel2.Controls.Add(this.label314);
            this.panel2.Controls.Add(this.label75);
            this.panel2.Controls.Add(this.label313);
            this.panel2.Controls.Add(this.label207);
            this.panel2.Controls.Add(this.label312);
            this.panel2.Controls.Add(this.label71);
            this.panel2.Controls.Add(this.label311);
            this.panel2.Controls.Add(this.label206);
            this.panel2.Controls.Add(this.label310);
            this.panel2.Controls.Add(this.label67);
            this.panel2.Controls.Add(this.label309);
            this.panel2.Controls.Add(this.label205);
            this.panel2.Controls.Add(this.label308);
            this.panel2.Controls.Add(this.label63);
            this.panel2.Controls.Add(this.label307);
            this.panel2.Controls.Add(this.label204);
            this.panel2.Controls.Add(this.label306);
            this.panel2.Controls.Add(this.label304);
            this.panel2.Controls.Add(this.label59);
            this.panel2.Controls.Add(this.label55);
            this.panel2.Controls.Add(this.label305);
            this.panel2.Controls.Add(this.label303);
            this.panel2.Controls.Add(this.label203);
            this.panel2.Controls.Add(this.label202);
            this.panel2.Controls.Add(this.label302);
            this.panel2.Controls.Add(this.label52);
            this.panel2.Controls.Add(this.label301);
            this.panel2.Controls.Add(this.label201);
            this.panel2.Controls.Add(this.label38);
            this.panel2.Controls.Add(this.label28);
            this.panel2.Controls.Add(this.label27);
            this.panel2.Controls.Add(this.label26);
            this.panel2.Controls.Add(this.label25);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Location = new System.Drawing.Point(37, 233);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1511, 289);
            this.panel2.TabIndex = 1;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label37.Location = new System.Drawing.Point(621, 218);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(196, 19);
            this.label37.TabIndex = 85;
            this.label37.Text = "23.11.02 08:00:00";
            this.label37.Click += new System.EventHandler(this.label37_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(621, 177);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 12);
            this.label11.TabIndex = 84;
            this.label11.Text = "质检时间";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button2.Location = new System.Drawing.Point(417, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(94, 32);
            this.button2.TabIndex = 83;
            this.button2.Text = "发送光谱数据";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // label328
            // 
            this.label328.AutoSize = true;
            this.label328.Location = new System.Drawing.Point(543, 259);
            this.label328.Name = "label328";
            this.label328.Size = new System.Drawing.Size(23, 12);
            this.label328.TabIndex = 81;
            this.label328.Text = "4.0";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(524, 259);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(11, 12);
            this.label103.TabIndex = 80;
            this.label103.Text = "-";
            // 
            // label327
            // 
            this.label327.AutoSize = true;
            this.label327.Location = new System.Drawing.Point(493, 259);
            this.label327.Name = "label327";
            this.label327.Size = new System.Drawing.Size(23, 12);
            this.label327.TabIndex = 79;
            this.label327.Text = "3.7";
            // 
            // label214
            // 
            this.label214.AutoSize = true;
            this.label214.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label214.Location = new System.Drawing.Point(493, 219);
            this.label214.Name = "label214";
            this.label214.Size = new System.Drawing.Size(53, 19);
            this.label214.TabIndex = 78;
            this.label214.Text = "3.75";
            // 
            // label326
            // 
            this.label326.AutoSize = true;
            this.label326.Location = new System.Drawing.Point(418, 259);
            this.label326.Name = "label326";
            this.label326.Size = new System.Drawing.Size(23, 12);
            this.label326.TabIndex = 77;
            this.label326.Text = "4.0";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(399, 259);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(11, 12);
            this.label99.TabIndex = 76;
            this.label99.Text = "-";
            // 
            // label325
            // 
            this.label325.AutoSize = true;
            this.label325.Location = new System.Drawing.Point(368, 259);
            this.label325.Name = "label325";
            this.label325.Size = new System.Drawing.Size(23, 12);
            this.label325.TabIndex = 75;
            this.label325.Text = "3.7";
            // 
            // label213
            // 
            this.label213.AutoSize = true;
            this.label213.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label213.Location = new System.Drawing.Point(368, 219);
            this.label213.Name = "label213";
            this.label213.Size = new System.Drawing.Size(53, 19);
            this.label213.TabIndex = 74;
            this.label213.Text = "3.75";
            // 
            // label324
            // 
            this.label324.AutoSize = true;
            this.label324.Location = new System.Drawing.Point(310, 259);
            this.label324.Name = "label324";
            this.label324.Size = new System.Drawing.Size(23, 12);
            this.label324.TabIndex = 73;
            this.label324.Text = "4.0";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(291, 259);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(11, 12);
            this.label95.TabIndex = 72;
            this.label95.Text = "-";
            // 
            // label323
            // 
            this.label323.AutoSize = true;
            this.label323.Location = new System.Drawing.Point(260, 259);
            this.label323.Name = "label323";
            this.label323.Size = new System.Drawing.Size(23, 12);
            this.label323.TabIndex = 71;
            this.label323.Text = "3.7";
            // 
            // label212
            // 
            this.label212.AutoSize = true;
            this.label212.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label212.Location = new System.Drawing.Point(260, 219);
            this.label212.Name = "label212";
            this.label212.Size = new System.Drawing.Size(53, 19);
            this.label212.TabIndex = 70;
            this.label212.Text = "3.75";
            // 
            // label322
            // 
            this.label322.AutoSize = true;
            this.label322.Location = new System.Drawing.Point(215, 259);
            this.label322.Name = "label322";
            this.label322.Size = new System.Drawing.Size(23, 12);
            this.label322.TabIndex = 69;
            this.label322.Text = "4.0";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(196, 259);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(11, 12);
            this.label91.TabIndex = 68;
            this.label91.Text = "-";
            // 
            // label321
            // 
            this.label321.AutoSize = true;
            this.label321.Location = new System.Drawing.Point(165, 259);
            this.label321.Name = "label321";
            this.label321.Size = new System.Drawing.Size(23, 12);
            this.label321.TabIndex = 67;
            this.label321.Text = "3.7";
            // 
            // label211
            // 
            this.label211.AutoSize = true;
            this.label211.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label211.Location = new System.Drawing.Point(165, 219);
            this.label211.Name = "label211";
            this.label211.Size = new System.Drawing.Size(53, 19);
            this.label211.TabIndex = 66;
            this.label211.Text = "3.75";
            // 
            // label320
            // 
            this.label320.AutoSize = true;
            this.label320.Location = new System.Drawing.Point(1305, 134);
            this.label320.Name = "label320";
            this.label320.Size = new System.Drawing.Size(23, 12);
            this.label320.TabIndex = 65;
            this.label320.Text = "4.0";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(1286, 134);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(11, 12);
            this.label87.TabIndex = 64;
            this.label87.Text = "-";
            // 
            // label319
            // 
            this.label319.AutoSize = true;
            this.label319.Location = new System.Drawing.Point(1255, 134);
            this.label319.Name = "label319";
            this.label319.Size = new System.Drawing.Size(23, 12);
            this.label319.TabIndex = 63;
            this.label319.Text = "3.7";
            // 
            // label210
            // 
            this.label210.AutoSize = true;
            this.label210.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label210.Location = new System.Drawing.Point(1255, 94);
            this.label210.Name = "label210";
            this.label210.Size = new System.Drawing.Size(53, 19);
            this.label210.TabIndex = 62;
            this.label210.Text = "3.75";
            // 
            // label318
            // 
            this.label318.AutoSize = true;
            this.label318.Location = new System.Drawing.Point(1164, 134);
            this.label318.Name = "label318";
            this.label318.Size = new System.Drawing.Size(23, 12);
            this.label318.TabIndex = 61;
            this.label318.Text = "4.0";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(1145, 134);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(11, 12);
            this.label83.TabIndex = 60;
            this.label83.Text = "-";
            // 
            // label317
            // 
            this.label317.AutoSize = true;
            this.label317.Location = new System.Drawing.Point(1114, 134);
            this.label317.Name = "label317";
            this.label317.Size = new System.Drawing.Size(23, 12);
            this.label317.TabIndex = 59;
            this.label317.Text = "3.7";
            // 
            // label209
            // 
            this.label209.AutoSize = true;
            this.label209.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label209.Location = new System.Drawing.Point(1114, 94);
            this.label209.Name = "label209";
            this.label209.Size = new System.Drawing.Size(53, 19);
            this.label209.TabIndex = 58;
            this.label209.Text = "3.75";
            // 
            // label316
            // 
            this.label316.AutoSize = true;
            this.label316.Location = new System.Drawing.Point(1032, 134);
            this.label316.Name = "label316";
            this.label316.Size = new System.Drawing.Size(23, 12);
            this.label316.TabIndex = 57;
            this.label316.Text = "4.0";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(1013, 134);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(11, 12);
            this.label79.TabIndex = 56;
            this.label79.Text = "-";
            // 
            // label315
            // 
            this.label315.AutoSize = true;
            this.label315.Location = new System.Drawing.Point(982, 134);
            this.label315.Name = "label315";
            this.label315.Size = new System.Drawing.Size(23, 12);
            this.label315.TabIndex = 55;
            this.label315.Text = "3.7";
            // 
            // label208
            // 
            this.label208.AutoSize = true;
            this.label208.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label208.Location = new System.Drawing.Point(982, 94);
            this.label208.Name = "label208";
            this.label208.Size = new System.Drawing.Size(53, 19);
            this.label208.TabIndex = 54;
            this.label208.Text = "3.15";
            // 
            // label314
            // 
            this.label314.AutoSize = true;
            this.label314.Location = new System.Drawing.Point(923, 134);
            this.label314.Name = "label314";
            this.label314.Size = new System.Drawing.Size(23, 12);
            this.label314.TabIndex = 53;
            this.label314.Text = "4.0";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(904, 134);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(11, 12);
            this.label75.TabIndex = 52;
            this.label75.Text = "-";
            // 
            // label313
            // 
            this.label313.AutoSize = true;
            this.label313.Location = new System.Drawing.Point(873, 134);
            this.label313.Name = "label313";
            this.label313.Size = new System.Drawing.Size(23, 12);
            this.label313.TabIndex = 51;
            this.label313.Text = "3.7";
            // 
            // label207
            // 
            this.label207.AutoSize = true;
            this.label207.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label207.Location = new System.Drawing.Point(873, 94);
            this.label207.Name = "label207";
            this.label207.Size = new System.Drawing.Size(53, 19);
            this.label207.TabIndex = 50;
            this.label207.Text = "3.73";
            // 
            // label312
            // 
            this.label312.AutoSize = true;
            this.label312.Location = new System.Drawing.Point(813, 134);
            this.label312.Name = "label312";
            this.label312.Size = new System.Drawing.Size(23, 12);
            this.label312.TabIndex = 49;
            this.label312.Text = "4.0";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(794, 134);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(11, 12);
            this.label71.TabIndex = 48;
            this.label71.Text = "-";
            // 
            // label311
            // 
            this.label311.AutoSize = true;
            this.label311.Location = new System.Drawing.Point(763, 134);
            this.label311.Name = "label311";
            this.label311.Size = new System.Drawing.Size(23, 12);
            this.label311.TabIndex = 47;
            this.label311.Text = "3.7";
            // 
            // label206
            // 
            this.label206.AutoSize = true;
            this.label206.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label206.Location = new System.Drawing.Point(763, 94);
            this.label206.Name = "label206";
            this.label206.Size = new System.Drawing.Size(42, 19);
            this.label206.TabIndex = 46;
            this.label206.Text = "4.3";
            // 
            // label310
            // 
            this.label310.AutoSize = true;
            this.label310.Location = new System.Drawing.Point(669, 134);
            this.label310.Name = "label310";
            this.label310.Size = new System.Drawing.Size(23, 12);
            this.label310.TabIndex = 45;
            this.label310.Text = "4.0";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(650, 134);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(11, 12);
            this.label67.TabIndex = 44;
            this.label67.Text = "-";
            // 
            // label309
            // 
            this.label309.AutoSize = true;
            this.label309.Location = new System.Drawing.Point(619, 134);
            this.label309.Name = "label309";
            this.label309.Size = new System.Drawing.Size(23, 12);
            this.label309.TabIndex = 43;
            this.label309.Text = "3.7";
            // 
            // label205
            // 
            this.label205.AutoSize = true;
            this.label205.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label205.Location = new System.Drawing.Point(619, 94);
            this.label205.Name = "label205";
            this.label205.Size = new System.Drawing.Size(53, 19);
            this.label205.TabIndex = 42;
            this.label205.Text = "3.75";
            // 
            // label308
            // 
            this.label308.AutoSize = true;
            this.label308.Location = new System.Drawing.Point(543, 134);
            this.label308.Name = "label308";
            this.label308.Size = new System.Drawing.Size(23, 12);
            this.label308.TabIndex = 41;
            this.label308.Text = "4.0";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(524, 134);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(11, 12);
            this.label63.TabIndex = 40;
            this.label63.Text = "-";
            // 
            // label307
            // 
            this.label307.AutoSize = true;
            this.label307.Location = new System.Drawing.Point(493, 134);
            this.label307.Name = "label307";
            this.label307.Size = new System.Drawing.Size(23, 12);
            this.label307.TabIndex = 39;
            this.label307.Text = "3.7";
            // 
            // label204
            // 
            this.label204.AutoSize = true;
            this.label204.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label204.Location = new System.Drawing.Point(493, 94);
            this.label204.Name = "label204";
            this.label204.Size = new System.Drawing.Size(53, 19);
            this.label204.TabIndex = 38;
            this.label204.Text = "3.75";
            // 
            // label306
            // 
            this.label306.AutoSize = true;
            this.label306.Location = new System.Drawing.Point(419, 134);
            this.label306.Name = "label306";
            this.label306.Size = new System.Drawing.Size(23, 12);
            this.label306.TabIndex = 37;
            this.label306.Text = "4.0";
            // 
            // label304
            // 
            this.label304.AutoSize = true;
            this.label304.Location = new System.Drawing.Point(310, 134);
            this.label304.Name = "label304";
            this.label304.Size = new System.Drawing.Size(23, 12);
            this.label304.TabIndex = 37;
            this.label304.Text = "4.0";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(400, 134);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(11, 12);
            this.label59.TabIndex = 36;
            this.label59.Text = "-";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(291, 134);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(11, 12);
            this.label55.TabIndex = 36;
            this.label55.Text = "-";
            // 
            // label305
            // 
            this.label305.AutoSize = true;
            this.label305.Location = new System.Drawing.Point(369, 134);
            this.label305.Name = "label305";
            this.label305.Size = new System.Drawing.Size(23, 12);
            this.label305.TabIndex = 35;
            this.label305.Text = "3.7";
            // 
            // label303
            // 
            this.label303.AutoSize = true;
            this.label303.Location = new System.Drawing.Point(260, 134);
            this.label303.Name = "label303";
            this.label303.Size = new System.Drawing.Size(23, 12);
            this.label303.TabIndex = 35;
            this.label303.Text = "3.7";
            // 
            // label203
            // 
            this.label203.AutoSize = true;
            this.label203.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label203.Location = new System.Drawing.Point(369, 94);
            this.label203.Name = "label203";
            this.label203.Size = new System.Drawing.Size(53, 19);
            this.label203.TabIndex = 34;
            this.label203.Text = "3.75";
            // 
            // label202
            // 
            this.label202.AutoSize = true;
            this.label202.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label202.Location = new System.Drawing.Point(260, 94);
            this.label202.Name = "label202";
            this.label202.Size = new System.Drawing.Size(53, 19);
            this.label202.TabIndex = 34;
            this.label202.Text = "3.75";
            this.label202.Click += new System.EventHandler(this.label202_Click);
            // 
            // label302
            // 
            this.label302.AutoSize = true;
            this.label302.Location = new System.Drawing.Point(215, 134);
            this.label302.Name = "label302";
            this.label302.Size = new System.Drawing.Size(23, 12);
            this.label302.TabIndex = 33;
            this.label302.Text = "4.0";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(196, 134);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(11, 12);
            this.label52.TabIndex = 32;
            this.label52.Text = "-";
            // 
            // label301
            // 
            this.label301.AutoSize = true;
            this.label301.Location = new System.Drawing.Point(165, 134);
            this.label301.Name = "label301";
            this.label301.Size = new System.Drawing.Size(23, 12);
            this.label301.TabIndex = 31;
            this.label301.Text = "3.7";
            this.label301.Click += new System.EventHandler(this.label301_Click);
            // 
            // label201
            // 
            this.label201.AutoSize = true;
            this.label201.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label201.Location = new System.Drawing.Point(165, 94);
            this.label201.Name = "label201";
            this.label201.Size = new System.Drawing.Size(53, 19);
            this.label201.TabIndex = 30;
            this.label201.Text = "3.75";
            this.label201.Click += new System.EventHandler(this.label201_Click);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(493, 177);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(11, 12);
            this.label38.TabIndex = 29;
            this.label38.Text = "V";
            this.label38.Click += new System.EventHandler(this.label38_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(93, 259);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(41, 12);
            this.label28.TabIndex = 28;
            this.label28.Text = "标准值";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(93, 134);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(41, 12);
            this.label27.TabIndex = 27;
            this.label27.Text = "标准值";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(93, 218);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(41, 12);
            this.label26.TabIndex = 26;
            this.label26.Text = "实际值";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(93, 176);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(41, 12);
            this.label25.TabIndex = 25;
            this.label25.Text = "检测项";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(369, 177);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(17, 12);
            this.label18.TabIndex = 20;
            this.label18.Text = "Ti";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(263, 177);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(17, 12);
            this.label19.TabIndex = 21;
            this.label19.Text = "Al";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(165, 177);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(17, 12);
            this.label20.TabIndex = 22;
            this.label20.Text = "Mo";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(1255, 56);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(17, 12);
            this.label21.TabIndex = 24;
            this.label21.Text = "Cu";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(1114, 55);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(17, 12);
            this.label22.TabIndex = 23;
            this.label22.Text = "Ni";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(982, 56);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(17, 12);
            this.label23.TabIndex = 19;
            this.label23.Text = "Cr";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(873, 55);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(17, 12);
            this.label24.TabIndex = 18;
            this.label24.Text = "Mg";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(763, 56);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(23, 12);
            this.label17.TabIndex = 16;
            this.label17.Text = "CEQ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(619, 55);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(11, 12);
            this.label16.TabIndex = 16;
            this.label16.Text = "P";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(493, 55);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(11, 12);
            this.label15.TabIndex = 16;
            this.label15.Text = "S";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(369, 55);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 12);
            this.label14.TabIndex = 17;
            this.label14.Text = "Mn";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(260, 56);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(17, 12);
            this.label13.TabIndex = 16;
            this.label13.Text = "Si";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(165, 56);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(11, 12);
            this.label12.TabIndex = 15;
            this.label12.Text = "C";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(93, 94);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 12);
            this.label10.TabIndex = 13;
            this.label10.Text = "实际值";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(93, 55);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 12);
            this.label9.TabIndex = 12;
            this.label9.Text = "检测项";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 13);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 12);
            this.label8.TabIndex = 11;
            this.label8.Text = "铁水光谱质检";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.序号,
            this.车间,
            this.炉次,
            this.炉次号,
            this.是否球化,
            this.操作员,
            this.时间,
            this.C,
            this.Si,
            this.Mn,
            this.S,
            this.P,
            this.CEQ,
            this.Mg,
            this.Cr,
            this.Ni,
            this.Cu,
            this.Mo,
            this.Al,
            this.Ti,
            this.V,
            this.Ce});
            this.dataGridView1.Location = new System.Drawing.Point(67, 66);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 52;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(1381, 251);
            this.dataGridView1.TabIndex = 13;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick_2);
            // 
            // 序号
            // 
            this.序号.DataPropertyName = "id";
            this.序号.HeaderText = "序号";
            this.序号.MinimumWidth = 6;
            this.序号.Name = "序号";
            this.序号.Width = 126;
            // 
            // 车间
            // 
            this.车间.DataPropertyName = "cj";
            this.车间.HeaderText = "车间";
            this.车间.MinimumWidth = 6;
            this.车间.Name = "车间";
            this.车间.Width = 126;
            // 
            // 炉次
            // 
            this.炉次.DataPropertyName = "lc";
            this.炉次.HeaderText = "炉次";
            this.炉次.MinimumWidth = 6;
            this.炉次.Name = "炉次";
            this.炉次.Width = 126;
            // 
            // 炉次号
            // 
            this.炉次号.DataPropertyName = "lch";
            this.炉次号.HeaderText = "炉次号";
            this.炉次号.MinimumWidth = 6;
            this.炉次号.Name = "炉次号";
            this.炉次号.Width = 126;
            // 
            // 是否球化
            // 
            this.是否球化.DataPropertyName = "qh_if";
            this.是否球化.HeaderText = "球化前/后";
            this.是否球化.MinimumWidth = 6;
            this.是否球化.Name = "是否球化";
            this.是否球化.Width = 126;
            // 
            // 操作员
            // 
            this.操作员.DataPropertyName = "czy";
            this.操作员.HeaderText = "操作员";
            this.操作员.MinimumWidth = 6;
            this.操作员.Name = "操作员";
            this.操作员.Width = 126;
            // 
            // 时间
            // 
            this.时间.DataPropertyName = "sj";
            this.时间.HeaderText = "时间";
            this.时间.MinimumWidth = 6;
            this.时间.Name = "时间";
            this.时间.Width = 126;
            // 
            // C
            // 
            this.C.DataPropertyName = "C";
            this.C.HeaderText = "C";
            this.C.MinimumWidth = 6;
            this.C.Name = "C";
            this.C.Width = 126;
            // 
            // Si
            // 
            this.Si.DataPropertyName = "Si";
            this.Si.HeaderText = "Si";
            this.Si.MinimumWidth = 6;
            this.Si.Name = "Si";
            this.Si.Width = 126;
            // 
            // Mn
            // 
            this.Mn.DataPropertyName = "Mn";
            this.Mn.HeaderText = "Mn";
            this.Mn.MinimumWidth = 6;
            this.Mn.Name = "Mn";
            this.Mn.Width = 126;
            // 
            // S
            // 
            this.S.DataPropertyName = "S";
            this.S.HeaderText = "S";
            this.S.MinimumWidth = 6;
            this.S.Name = "S";
            this.S.Width = 126;
            // 
            // P
            // 
            this.P.DataPropertyName = "P";
            this.P.HeaderText = "P";
            this.P.MinimumWidth = 6;
            this.P.Name = "P";
            this.P.Width = 126;
            // 
            // CEQ
            // 
            this.CEQ.DataPropertyName = "CEQ";
            this.CEQ.HeaderText = "CEQ";
            this.CEQ.MinimumWidth = 6;
            this.CEQ.Name = "CEQ";
            this.CEQ.Width = 126;
            // 
            // Mg
            // 
            this.Mg.DataPropertyName = "Mg";
            this.Mg.HeaderText = "Mg";
            this.Mg.MinimumWidth = 6;
            this.Mg.Name = "Mg";
            this.Mg.Width = 126;
            // 
            // Cr
            // 
            this.Cr.DataPropertyName = "Cr";
            this.Cr.HeaderText = "Cr";
            this.Cr.MinimumWidth = 6;
            this.Cr.Name = "Cr";
            this.Cr.Width = 126;
            // 
            // Ni
            // 
            this.Ni.DataPropertyName = "Ni";
            this.Ni.HeaderText = "Ni";
            this.Ni.MinimumWidth = 6;
            this.Ni.Name = "Ni";
            this.Ni.Width = 126;
            // 
            // Cu
            // 
            this.Cu.DataPropertyName = "Cu";
            this.Cu.HeaderText = "Cu";
            this.Cu.MinimumWidth = 6;
            this.Cu.Name = "Cu";
            this.Cu.Width = 126;
            // 
            // Mo
            // 
            this.Mo.DataPropertyName = "Mo";
            this.Mo.HeaderText = "Mo";
            this.Mo.MinimumWidth = 6;
            this.Mo.Name = "Mo";
            this.Mo.Width = 126;
            // 
            // Al
            // 
            this.Al.DataPropertyName = "Al";
            this.Al.HeaderText = "Al";
            this.Al.MinimumWidth = 6;
            this.Al.Name = "Al";
            this.Al.Width = 126;
            // 
            // Ti
            // 
            this.Ti.DataPropertyName = "Ti";
            this.Ti.HeaderText = "Ti";
            this.Ti.MinimumWidth = 6;
            this.Ti.Name = "Ti";
            this.Ti.Width = 126;
            // 
            // V
            // 
            this.V.DataPropertyName = "V";
            this.V.HeaderText = "V";
            this.V.MinimumWidth = 6;
            this.V.Name = "V";
            this.V.Width = 126;
            // 
            // Ce
            // 
            this.Ce.DataPropertyName = "Ce";
            this.Ce.HeaderText = "Ce";
            this.Ce.MinimumWidth = 6;
            this.Ce.Name = "Ce";
            this.Ce.Width = 126;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(37, 13);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(29, 12);
            this.label32.TabIndex = 2;
            this.label32.Text = "首页";
            this.label32.Click += new System.EventHandler(this.label32_Click);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(103, 13);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(53, 12);
            this.label33.TabIndex = 3;
            this.label33.Text = "配料管理";
            this.label33.Click += new System.EventHandler(this.label33_Click);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(197, 13);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(53, 12);
            this.label34.TabIndex = 4;
            this.label34.Text = "设备管理";
            this.label34.Click += new System.EventHandler(this.label34_Click);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(299, 13);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(83, 12);
            this.label35.TabIndex = 5;
            this.label35.Text = "LED屏显示管理";
            this.label35.Click += new System.EventHandler(this.label35_Click);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(422, 13);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(53, 12);
            this.label36.TabIndex = 6;
            this.label36.Text = "预警管理";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(17, 9);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(77, 12);
            this.label39.TabIndex = 7;
            this.label39.Text = "数据记录管理";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 12;
            this.listBox1.Location = new System.Drawing.Point(3, 12);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(234, 184);
            this.listBox1.TabIndex = 0;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.listBox2);
            this.panel3.Controls.Add(this.listBox1);
            this.panel3.Location = new System.Drawing.Point(555, 28);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(513, 199);
            this.panel3.TabIndex = 8;
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.ItemHeight = 12;
            this.listBox2.Items.AddRange(new object[] {
            "23.11.02 08:00:09 ：合金配料仪数据接收成功！"});
            this.listBox2.Location = new System.Drawing.Point(243, 12);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(267, 184);
            this.listBox2.TabIndex = 1;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.dateTimePicker2);
            this.panel4.Controls.Add(this.dateTimePicker1);
            this.panel4.Controls.Add(this.dataGridView1);
            this.panel4.Controls.Add(this.button1);
            this.panel4.Controls.Add(this.label30);
            this.panel4.Controls.Add(this.label29);
            this.panel4.Controls.Add(this.label39);
            this.panel4.Location = new System.Drawing.Point(37, 528);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1511, 330);
            this.panel4.TabIndex = 10;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(386, 33);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(150, 21);
            this.dateTimePicker2.TabIndex = 15;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(169, 33);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(150, 21);
            this.dateTimePicker1.TabIndex = 14;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(571, 31);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "查询";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(345, 37);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(29, 12);
            this.label30.TabIndex = 10;
            this.label30.Text = "至：";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(76, 37);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(77, 12);
            this.label29.TabIndex = 8;
            this.label29.Text = "生产日期从：";
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.label48);
            this.panel5.Controls.Add(this.label49);
            this.panel5.Controls.Add(this.label50);
            this.panel5.Controls.Add(this.label51);
            this.panel5.Controls.Add(this.label44);
            this.panel5.Controls.Add(this.label45);
            this.panel5.Controls.Add(this.label46);
            this.panel5.Controls.Add(this.label47);
            this.panel5.Controls.Add(this.label43);
            this.panel5.Controls.Add(this.label42);
            this.panel5.Controls.Add(this.label41);
            this.panel5.Controls.Add(this.label31);
            this.panel5.Controls.Add(this.label40);
            this.panel5.Location = new System.Drawing.Point(1074, 40);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(474, 187);
            this.panel5.TabIndex = 11;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(326, 156);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(17, 12);
            this.label48.TabIndex = 14;
            this.label48.Text = "Kg";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(326, 119);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(17, 12);
            this.label49.TabIndex = 13;
            this.label49.Text = "Kg";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(326, 84);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(17, 12);
            this.label50.TabIndex = 12;
            this.label50.Text = "Kg";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(326, 47);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(17, 12);
            this.label51.TabIndex = 11;
            this.label51.Text = "Kg";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label44.Location = new System.Drawing.Point(232, 156);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(31, 19);
            this.label44.TabIndex = 10;
            this.label44.Text = "22";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label45.Location = new System.Drawing.Point(232, 119);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(31, 19);
            this.label45.TabIndex = 9;
            this.label45.Text = "22";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label46.Location = new System.Drawing.Point(232, 84);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(31, 19);
            this.label46.TabIndex = 8;
            this.label46.Text = "22";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label47.Location = new System.Drawing.Point(232, 47);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(31, 19);
            this.label47.TabIndex = 7;
            this.label47.Text = "22";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(113, 156);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(29, 12);
            this.label43.TabIndex = 6;
            this.label43.Text = "硅粒";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(113, 119);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(29, 12);
            this.label42.TabIndex = 5;
            this.label42.Text = "锰铁";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(113, 84);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(41, 12);
            this.label41.TabIndex = 4;
            this.label41.Text = "增碳剂";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(113, 47);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(53, 12);
            this.label31.TabIndex = 2;
            this.label31.Text = "铁水重量";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(16, 14);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(89, 12);
            this.label40.TabIndex = 1;
            this.label40.Text = "合金配料仪数据";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1407, 705);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Main";
            this.Text = "济南科德智能科技有限公司-光谱检测管理系统";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Panel panel3;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DataGridViewTextBoxColumn 序号;
        private System.Windows.Forms.DataGridViewTextBoxColumn 车间;
        private System.Windows.Forms.DataGridViewTextBoxColumn 炉次;
        private System.Windows.Forms.DataGridViewTextBoxColumn 炉次号;
        private System.Windows.Forms.DataGridViewTextBoxColumn 是否球化;
        private System.Windows.Forms.DataGridViewTextBoxColumn 操作员;
        private System.Windows.Forms.DataGridViewTextBoxColumn 时间;
        private System.Windows.Forms.DataGridViewTextBoxColumn C;
        private System.Windows.Forms.DataGridViewTextBoxColumn Si;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mn;
        private System.Windows.Forms.DataGridViewTextBoxColumn S;
        private System.Windows.Forms.DataGridViewTextBoxColumn P;
        private System.Windows.Forms.DataGridViewTextBoxColumn CEQ;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mg;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cr;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ni;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cu;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Al;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ti;
        private System.Windows.Forms.DataGridViewTextBoxColumn V;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ce;
        private System.Windows.Forms.Label label201;
        private System.Windows.Forms.Label label320;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label319;
        private System.Windows.Forms.Label label210;
        private System.Windows.Forms.Label label318;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label317;
        private System.Windows.Forms.Label label209;
        private System.Windows.Forms.Label label316;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label315;
        private System.Windows.Forms.Label label208;
        private System.Windows.Forms.Label label314;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label313;
        private System.Windows.Forms.Label label207;
        private System.Windows.Forms.Label label312;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label311;
        private System.Windows.Forms.Label label206;
        private System.Windows.Forms.Label label310;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label309;
        private System.Windows.Forms.Label label205;
        private System.Windows.Forms.Label label308;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label307;
        private System.Windows.Forms.Label label204;
        private System.Windows.Forms.Label label306;
        private System.Windows.Forms.Label label304;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label305;
        private System.Windows.Forms.Label label303;
        private System.Windows.Forms.Label label203;
        private System.Windows.Forms.Label label202;
        private System.Windows.Forms.Label label302;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label301;
        private System.Windows.Forms.Label label322;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label321;
        private System.Windows.Forms.Label label211;
        private System.Windows.Forms.Label label328;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label327;
        private System.Windows.Forms.Label label214;
        private System.Windows.Forms.Label label326;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label325;
        private System.Windows.Forms.Label label213;
        private System.Windows.Forms.Label label324;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label323;
        private System.Windows.Forms.Label label212;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button3;
    }
}

