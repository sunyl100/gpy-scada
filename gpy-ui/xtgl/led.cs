﻿using gpy_common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gpy_ui.xtgl
{
    public partial class led : Form
    {
        public led()
        {
            InitializeComponent();
            search();
        }

        private void led_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            search();
        }
        public void search()
        {
            var cj = textBox1.Text;
            var connect = Global.Instance.sqlLiteHelper.sqliteConnection;

            var where = "";
            if (cj.Trim().Length != 0)
            {
                where = " where cj = '" + cj + "'";
            }
            String order = " order by id desc ";

            int total = 0;
            SQLiteDataReader reader1 = Global.Instance.sqlLiteHelper.ExecuteQuery("select count(*) as c FROM sys_led " + where);
            while (reader1.Read())
            {
                if (!reader1.IsDBNull(reader1.GetOrdinal("c")))
                {
                    total = reader1.GetInt32(reader1.GetOrdinal("c"));
                }
            }

            int pageSize = int.Parse(comboBox1.SelectedItem == null ? "10" : comboBox1.SelectedItem.ToString());
            int curentPage = int.Parse(label7.Text);
            int pages = Decimal.ToInt32(Math.Ceiling(new Decimal(total / pageSize))) + 1;
            label3.Text = pages + "";
            //下一页是否显示 控制下一页  
            if (total > pageSize * curentPage)
            {
                label6.Show();
            }
            else
            {
                label6.Hide();
            }

            var Datatable = new DataTable();
            String limit = " limit " + (curentPage - 1) * pageSize + "," + pageSize;
            String q = "select * from sys_led" + where  + order + limit;
            Console.WriteLine(q);
            var adp = new SQLiteDataAdapter(q, connect);
            adp.Fill(Datatable);
            dataGridView1.DataSource = Datatable;
        }



        private void 新增_Click(object sender, EventArgs e)
        {
            new led_add(this).ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (dataGridView1.SelectedRows.Count > 0)
            {
                int id = int.Parse(dataGridView1.SelectedRows[0].Cells[0].Value.ToString());
                new led_edit(id,this).ShowDialog();
            }
            else
            {
                MessageBox.Show("请先选择一条记录");
                return;
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                var id = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                if (MessageBox.Show("你确定要删除该记录吗", "询问", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    Global.Instance.sqlLiteHelper.ExecuteQuery("DELETE FROM sys_led WHERE id = " + id);
                    MessageBox.Show("删除成功！");
                    search();
                }

            }
            else
            {
                MessageBox.Show("请先选择一条记录");
                return;
            }
        }

        private void 重置_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
