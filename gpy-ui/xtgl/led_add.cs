﻿using gpy_common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gpy_ui.xtgl
{
    public partial class led_add : Form
    {
        public led led;
        public led_add()
        {
            InitializeComponent();
        }
        public led_add(led led)
        {
            InitializeComponent();
            this.led = led;
        }


        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void save_Click(object sender, EventArgs e)
        {
            var cj = textBox1.Text;
            var model = textBox2.Text;
            var ip = textBox3.Text;
            var port = textBox4.Text;
            var msg_type = comboBox2.Text;
            var communicate_protocol = comboBox3.Text;
            var address = textBox6.Text;
            var remark = textBox5.Text;
            var values = new string[] { cj, model, ip, port, msg_type, communicate_protocol, address, remark };
            var clumns = new string[] { "cj", "model", "ip", "port", "msg_type", "communicate_protocol", "address", "remark" };

            if (!Valiad.ifnull(values))
            {
                MessageBox.Show("所有数据均为必填项！");
                return;
            }
            //插入数据
            Global.Instance.sqlLiteHelper.InsertValuesAutoId("sys_led", clumns, values);
            Console.WriteLine("写入 sys_led" + values.ToString());
            this.Close();
            MessageBox.Show("保存成功！");
            led.search();
        }
        public Boolean ifnull (string[] ss)
        {
            foreach (String val in ss)
            {
                if (val == null || val.Trim().Length == 0) {
                    return false;
                }
            }
            return true;
        }

        private void led_add_Load(object sender, EventArgs e)
        {

        }
    }
}
