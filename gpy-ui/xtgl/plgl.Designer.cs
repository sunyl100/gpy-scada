﻿
namespace gpy_ui.xtgl
{
    partial class plgl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.plgllist = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.ph = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.plgllist)).BeginInit();
            this.SuspendLayout();
            // 
            // plgllist
            // 
            this.plgllist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.plgllist.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ph,
            this.fg});
            this.plgllist.Location = new System.Drawing.Point(12, 70);
            this.plgllist.Name = "plgllist";
            this.plgllist.RowHeadersWidth = 52;
            this.plgllist.RowTemplate.Height = 27;
            this.plgllist.Size = new System.Drawing.Size(776, 357);
            this.plgllist.TabIndex = 0;
            this.plgllist.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button1.Location = new System.Drawing.Point(623, 25);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(109, 39);
            this.button1.TabIndex = 1;
            this.button1.Text = "查询";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ph
            // 
            this.ph.DataPropertyName = "ph";
            this.ph.HeaderText = "ph";
            this.ph.MinimumWidth = 6;
            this.ph.Name = "ph";
            this.ph.Width = 126;
            // 
            // fg
            // 
            this.fg.DataPropertyName = "fg";
            this.fg.HeaderText = "fg";
            this.fg.MinimumWidth = 6;
            this.fg.Name = "fg";
            this.fg.Width = 126;
            // 
            // plgl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.plgllist);
            this.Name = "plgl";
            this.Text = "plgl";
            this.Load += new System.EventHandler(this.plgl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.plgllist)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView plgllist;
        private System.Windows.Forms.DataGridViewTextBoxColumn ph;
        private System.Windows.Forms.DataGridViewTextBoxColumn fg;
        private System.Windows.Forms.Button button1;
    }
}