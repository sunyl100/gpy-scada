﻿using gpy_common;
using gpy_ui.xtgl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gpy_ui
{
    public partial class equipment_edit : Form
    {
        public equipment equipment;
        public equipment_edit()
        {
            InitializeComponent();
        }
        public equipment_edit(equipment equipment)
        {
            InitializeComponent();
            this.equipment = equipment;
        }
        public equipment_edit(int id, equipment equipment)
        {
            InitializeComponent();
            this.equipment = equipment;
            SQLiteDataReader reader1 = Global.Instance.sqlLiteHelper.ExecuteQuery("select *  FROM sys_equipment WHERE id = " + id);
            while (reader1.Read())
            {
                if (!reader1.IsDBNull(reader1.GetOrdinal("id")))
                {
                    id = reader1.GetInt32(reader1.GetOrdinal("id"));
                    textBox6.Text = id + "";
                }

                if (!reader1.IsDBNull(reader1.GetOrdinal("name")))
                {
                    textBox1.Text = reader1.GetString(reader1.GetOrdinal("factory"));
                }
                if (!reader1.IsDBNull(reader1.GetOrdinal("factory")))
                {
                    textBox2.Text = reader1.GetString(reader1.GetOrdinal("factory"));
                }
                if (!reader1.IsDBNull(reader1.GetOrdinal("model")))
                {
                    textBox3.Text = reader1.GetString(reader1.GetOrdinal("model"));
                }

                if (!reader1.IsDBNull(reader1.GetOrdinal("protocol")))
                {
                    comboBox1.Text = reader1.GetString(reader1.GetOrdinal("protocol"));
                }
                if (!reader1.IsDBNull(reader1.GetOrdinal("storage_type")))
                {
                    comboBox2.Text = reader1.GetString(reader1.GetOrdinal("storage_type"));
                }
                if (!reader1.IsDBNull(reader1.GetOrdinal("port")))
                {
                    textBox5.Text = reader1.GetString(reader1.GetOrdinal("port"));
                }
                if (!reader1.IsDBNull(reader1.GetOrdinal("ip")))
                {
                    textBox7.Text = reader1.GetString(reader1.GetOrdinal("ip"));
                } 
                
                if (!reader1.IsDBNull(reader1.GetOrdinal("storage_url")))
                {
                    textBox8.Text = reader1.GetString(reader1.GetOrdinal("storage_url"));
                }
            }

        }


        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form3_sbgl_Load(object sender, EventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var factory = textBox2.Text;
            var model = textBox3.Text;
            var protocol = comboBox1.Text;
            var storage_type = comboBox2.Text;
            var storage_url = textBox8.Text;
            var ip = textBox7.Text;
            var port = textBox5.Text;
            var id = textBox6.Text;
            var values = new string[] { name, factory, model, protocol, storage_type, storage_url, ip,port };
            var clumns = new string[] { "name", "factory", "model", "protocol", "storage_type", "storage_url" , "ip","port" };

            if (!Valiad.ifnull(values))
            {
                MessageBox.Show("所有数据均为必填项！");
                return;
            }
            //插入数据
            Global.Instance.sqlLiteHelper.UpdateValues("sys_equipment", clumns, values, "id", id, "=");
            MessageBox.Show("修改成功！");
            this.Close();
            Console.WriteLine("写入 sys_equipment" + values.ToString());
            equipment.search();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
