﻿using gpy_common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gpy_ui.xtgl
{
    public partial class equipment : Form
    {
        public equipment()
        {
            InitializeComponent();
            search();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            search();
        }

    public void search()
    {
        var name = textBox1.Text;
        var connect = Global.Instance.sqlLiteHelper.sqliteConnection;

        var where = "";
        if (name.Trim().Length != 0)
        {
            where = " where name = " + name;
        }
        String order = " order by id desc ";
        int total = 0;
        SQLiteDataReader reader1 = Global.Instance.sqlLiteHelper.ExecuteQuery("select count(*) as c FROM sys_equipment " + where );
        while (reader1.Read())
        {
            if (!reader1.IsDBNull(reader1.GetOrdinal("c")))
            {
                total = reader1.GetInt32(reader1.GetOrdinal("c"));
            }
        }

        int pageSize = int.Parse(comboBox1.SelectedItem == null ? "10" : comboBox1.SelectedItem.ToString());
        int curentPage = int.Parse(label5.Text);
        int pages = Decimal.ToInt32(Math.Ceiling(new Decimal(total / pageSize))) + 1;
        label2.Text = pages + "";//总页数
        if (total > pageSize * curentPage)
        {
            label7.Show();//下一页
        }
        else
        {
            label7.Hide();
        }

        var Datatable = new DataTable();
        String limit = " limit " + (curentPage - 1) * pageSize + "," + pageSize;
        String q = "select * from sys_equipment" + where + order + limit;
        Console.WriteLine(q);
        var adp = new SQLiteDataAdapter(q, connect);
        adp.Fill(Datatable);
        dataGridView1.DataSource = Datatable;
    }

        //新增
        private void button1_Click(object sender, EventArgs e)
        {
            //打开设备管理界面
            Form fm = new Form3_sbgl(this);
            fm.ShowDialog();
        }

        private void reset_Click(object sender, EventArgs e)
        {
           textBox1.Text = "";
        }

        private void edit_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                int id = int.Parse(dataGridView1.SelectedRows[0].Cells[0].Value.ToString());
                var edit = new equipment_edit(id,this);
                edit.ShowDialog();
            }
            else
            {
                MessageBox.Show("请先选择一条记录");
                return;
            }
            
        }

        private void del_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                var id = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                if (MessageBox.Show("你确定要删除该记录吗", "询问", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    Global.Instance.sqlLiteHelper.ExecuteQuery("DELETE FROM sys_equipment WHERE id = " + id);
                    MessageBox.Show("删除成功！");
                    search();
                }
            }
            else
            {
                MessageBox.Show("请先选择一条记录");
                return;
            }
        }
    }
}
