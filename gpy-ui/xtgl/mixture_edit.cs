﻿using gpy_common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gpy_ui.xtgl
{
    public partial class mixture_edit : Form
    {
        public mixture mixture;
        public mixture_edit()
        {
            InitializeComponent();
        }
        public mixture_edit(int id, mixture mixture)
        {
            InitializeComponent();
            this.mixture = mixture;
            SQLiteDataReader reader1 = Global.Instance.sqlLiteHelper.ExecuteQuery("select *  FROM sys_mixture WHERE id = " + id);
            while (reader1.Read())
            {
                if (!reader1.IsDBNull(reader1.GetOrdinal("id")))
                {
                    id = reader1.GetInt32(reader1.GetOrdinal("id"));
                    textBox6.Text = id + "";
                }

                if (!reader1.IsDBNull(reader1.GetOrdinal("mj")))
                {
                    textBox1.Text  =  reader1.GetString(reader1.GetOrdinal("mj"));
                }
                if (!reader1.IsDBNull(reader1.GetOrdinal("ph")))
                {
                    textBox2.Text = reader1.GetString(reader1.GetOrdinal("ph"));
                }
                if (!reader1.IsDBNull(reader1.GetOrdinal("lch")))
                {
                    textBox3.Text = reader1.GetString(reader1.GetOrdinal("lch"));
                }

                if (!reader1.IsDBNull(reader1.GetOrdinal("fg")))
                {
                    numericUpDown1.Text = reader1.GetDouble(reader1.GetOrdinal("fg")) + "";
                }
                if (!reader1.IsDBNull(reader1.GetOrdinal("hll")))
                {
                    numericUpDown2.Text = reader1.GetDouble(reader1.GetOrdinal("hll")) +"";
                }
                if (!reader1.IsDBNull(reader1.GetOrdinal("st")))
                {
                    numericUpDown3.Text = reader1.GetDouble(reader1.GetOrdinal("st")) +"";
                }

            }
            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var id = textBox6.Text;

            var mj = textBox1.Text;
            var ph = textBox2.Text;
            var lch = textBox3.Text;
            var fg = numericUpDown1.Text;
            var hll = numericUpDown2.Text;
            var st = numericUpDown3.Text;

            var values = new string[] { mj, ph, lch, fg, hll, st };
            var clumns = new string[] { "mj", "ph", "lch", "fg", "hll", "st" };
            if (!Valiad.ifnull(values))
            {
                MessageBox.Show("所有数据均为必填项！");
                return;
            }
            Global.Instance.sqlLiteHelper.UpdateValues("sys_mixture", clumns, values,"id",id, "=");
            MessageBox.Show("修改成功！");
            this.Close();
            mixture.search();

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void mixture_edit_Load(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
