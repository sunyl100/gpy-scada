﻿using gpy_common;
using gpy_ui.xtgl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gpy_ui
{
    public partial class mixture : Form
    {
        public mixture()
        {
            InitializeComponent();
            this.search();

        }
        
        private void mixture_Load(object sender, EventArgs e)
        {

        }
        //查询
        private void button1_Click(object sender, EventArgs e)
        {
            this.search();
        }

        public void search() {
            var ph = textBox1.Text;
            var connect = Global.Instance.sqlLiteHelper.sqliteConnection;
           
            var where = "";
            if (ph.Trim().Length != 0)
            {
                where = " where ph = " + ph;
            }
            String order = " order by id desc ";
            int total = 0;
            SQLiteDataReader reader1 = Global.Instance.sqlLiteHelper.ExecuteQuery("select count(*) as c FROM sys_mixture" + where + order);
            while (reader1.Read())
            {
                if (!reader1.IsDBNull(reader1.GetOrdinal("c")))
                {
                    total = reader1.GetInt32(reader1.GetOrdinal("c"));
                }
            }
            
            int pageSize = int.Parse(comboBox1.SelectedItem == null?"10": comboBox1.SelectedItem.ToString());
            int curentPage = int.Parse(label5.Text);
            int pages = Decimal.ToInt32(Math.Ceiling(new Decimal(total / pageSize))) + 1;
            label3.Text = pages + "";
            //下一页是否显示 控制下一页  
            if (total > pageSize * curentPage)
            {
                label6.Show();
            }
            else {
                label6.Hide();
            }

            var Datatable = new DataTable();
            String limit = " limit " + (curentPage - 1) * pageSize + "," + pageSize;
            var adp = new SQLiteDataAdapter("select * from sys_mixture" + where + order + limit, connect);
            adp.Fill(Datatable);
            配料管理.DataSource = Datatable;
        }
        public int count()
        {
            int c = 0;
            SQLiteDataReader reader1 = Global.Instance.sqlLiteHelper.ExecuteQuery("select count(*) as c FROM sys_mixture");
            while (reader1.Read())
            {
                if (!reader1.IsDBNull(reader1.GetOrdinal("c")))
                {
                    c = reader1.GetInt32(reader1.GetOrdinal("c"));
                    label3.Text = c + "";
                }
            }
            return c;
        }

        //重置
        private void button5_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
        }

        //新增
        private void button2_Click(object sender, EventArgs e)
        {
             new mixture_add(this).ShowDialog();
        }
        //编辑
        private void button3_Click(object sender, EventArgs e)
        {
            
            if (配料管理.SelectedRows.Count > 0)
            {
                int id  = int.Parse(配料管理.SelectedRows[0].Cells[0].Value.ToString());
                var edit = new mixture_edit(id,this);
                edit.ShowDialog();
            }
            else
            {
                MessageBox.Show("请先选择一条记录");
                return;
            }
        }
        //删除

        private void button4_Click(object sender, EventArgs e)
        {
            if (配料管理.SelectedRows.Count > 0)
            {
                var id = 配料管理.SelectedRows[0].Cells[0].Value.ToString();
                if (MessageBox.Show("你确定要删除该记录吗", "询问", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    Global.Instance.sqlLiteHelper.ExecuteQuery("DELETE FROM sys_mixture WHERE id = " + id);
                    MessageBox.Show("删除成功！");
                    search();
                }
               
            }
            else {
                MessageBox.Show("请先选择一条记录");
                return;
            }
           
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void 配料管理_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        //上一页 控制上一页
        private void label7_Click(object sender, EventArgs e)
        {

            if ((int.Parse(label5.Text) - 1) > 0)
            {
                label5.Text = (int.Parse(label5.Text) - 1) + "";
                this.search();
            }
            else {
                MessageBox.Show("已经是第一页");

            }
            
        }
        //下一页  
        private void label6_Click(object sender, EventArgs e)
        {
            label5.Text = (int.Parse(label5.Text) + 1) + "";
            this.search();
        }

        //总页数
        private void label3_Click(object sender, EventArgs e)
        {

        }
        //当前页
        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
           

        }
    }
}
