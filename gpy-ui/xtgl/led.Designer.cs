﻿
namespace gpy_ui.xtgl
{
    partial class led
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.新增 = new System.Windows.Forms.Button();
            this.编辑 = new System.Windows.Forms.Button();
            this.删除 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.查询 = new System.Windows.Forms.Button();
            this.重置 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cj = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.model = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ip = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.port = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.msg_type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.communicate_protocol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.remark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.cj,
            this.model,
            this.ip,
            this.port,
            this.msg_type,
            this.communicate_protocol,
            this.address,
            this.remark});
            this.dataGridView1.Location = new System.Drawing.Point(54, 82);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersWidth = 52;
            this.dataGridView1.RowTemplate.Height = 27;
            this.dataGridView1.Size = new System.Drawing.Size(1213, 455);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // 新增
            // 
            this.新增.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.新增.Location = new System.Drawing.Point(896, 26);
            this.新增.Name = "新增";
            this.新增.Size = new System.Drawing.Size(101, 36);
            this.新增.TabIndex = 1;
            this.新增.Text = "新增";
            this.新增.UseVisualStyleBackColor = false;
            this.新增.Click += new System.EventHandler(this.新增_Click);
            // 
            // 编辑
            // 
            this.编辑.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.编辑.Location = new System.Drawing.Point(1035, 26);
            this.编辑.Name = "编辑";
            this.编辑.Size = new System.Drawing.Size(101, 36);
            this.编辑.TabIndex = 2;
            this.编辑.Text = "编辑";
            this.编辑.UseVisualStyleBackColor = false;
            this.编辑.Click += new System.EventHandler(this.button1_Click);
            // 
            // 删除
            // 
            this.删除.BackColor = System.Drawing.Color.Pink;
            this.删除.Location = new System.Drawing.Point(1169, 26);
            this.删除.Name = "删除";
            this.删除.Size = new System.Drawing.Size(98, 36);
            this.删除.TabIndex = 3;
            this.删除.Text = "删除";
            this.删除.UseVisualStyleBackColor = false;
            this.删除.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(70, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "厂家";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(166, 25);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(150, 25);
            this.textBox1.TabIndex = 5;
            // 
            // 查询
            // 
            this.查询.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.查询.Location = new System.Drawing.Point(381, 26);
            this.查询.Name = "查询";
            this.查询.Size = new System.Drawing.Size(91, 36);
            this.查询.TabIndex = 6;
            this.查询.Text = "查询";
            this.查询.UseVisualStyleBackColor = false;
            this.查询.Click += new System.EventHandler(this.button3_Click);
            // 
            // 重置
            // 
            this.重置.BackColor = System.Drawing.SystemColors.HighlightText;
            this.重置.Location = new System.Drawing.Point(478, 26);
            this.重置.Name = "重置";
            this.重置.Size = new System.Drawing.Size(92, 36);
            this.重置.TabIndex = 7;
            this.重置.Text = "重置";
            this.重置.UseVisualStyleBackColor = false;
            this.重置.Click += new System.EventHandler(this.重置_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(249, 575);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 16);
            this.label2.TabIndex = 8;
            this.label2.Text = "总页数";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(400, 575);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 16);
            this.label4.TabIndex = 10;
            this.label4.Text = "当前页";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(822, 575);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 16);
            this.label5.TabIndex = 11;
            this.label5.Text = "上一页";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label6.Location = new System.Drawing.Point(928, 575);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 16);
            this.label6.TabIndex = 12;
            this.label6.Text = "下一页";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(327, 575);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 16);
            this.label3.TabIndex = 9;
            this.label3.Text = "1";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label7.Location = new System.Drawing.Point(475, 575);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(16, 16);
            this.label7.TabIndex = 13;
            this.label7.Text = "1";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "10",
            "20",
            "30"});
            this.comboBox1.Location = new System.Drawing.Point(1118, 568);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 23);
            this.comboBox1.TabIndex = 14;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "主键";
            this.id.MinimumWidth = 6;
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Width = 126;
            // 
            // cj
            // 
            this.cj.DataPropertyName = "cj";
            this.cj.HeaderText = "厂家";
            this.cj.MinimumWidth = 6;
            this.cj.Name = "cj";
            this.cj.ReadOnly = true;
            this.cj.Width = 126;
            // 
            // model
            // 
            this.model.DataPropertyName = "model";
            this.model.HeaderText = "厂家型号";
            this.model.MinimumWidth = 6;
            this.model.Name = "model";
            this.model.ReadOnly = true;
            this.model.Width = 126;
            // 
            // ip
            // 
            this.ip.DataPropertyName = "ip";
            this.ip.HeaderText = "ip";
            this.ip.MinimumWidth = 6;
            this.ip.Name = "ip";
            this.ip.ReadOnly = true;
            this.ip.Width = 126;
            // 
            // port
            // 
            this.port.DataPropertyName = "port";
            this.port.HeaderText = "端口";
            this.port.MinimumWidth = 6;
            this.port.Name = "port";
            this.port.ReadOnly = true;
            this.port.Width = 126;
            // 
            // msg_type
            // 
            this.msg_type.DataPropertyName = "msg_type";
            this.msg_type.HeaderText = "报文形式";
            this.msg_type.MinimumWidth = 6;
            this.msg_type.Name = "msg_type";
            this.msg_type.ReadOnly = true;
            this.msg_type.Width = 126;
            // 
            // communicate_protocol
            // 
            this.communicate_protocol.DataPropertyName = "communicate_protocol";
            this.communicate_protocol.HeaderText = "通讯协议";
            this.communicate_protocol.MinimumWidth = 6;
            this.communicate_protocol.Name = "communicate_protocol";
            this.communicate_protocol.ReadOnly = true;
            this.communicate_protocol.Width = 126;
            // 
            // address
            // 
            this.address.DataPropertyName = "address";
            this.address.HeaderText = "寄存器地址";
            this.address.MinimumWidth = 6;
            this.address.Name = "address";
            this.address.ReadOnly = true;
            this.address.Width = 126;
            // 
            // remark
            // 
            this.remark.DataPropertyName = "remark";
            this.remark.HeaderText = "备注";
            this.remark.MinimumWidth = 6;
            this.remark.Name = "remark";
            this.remark.ReadOnly = true;
            this.remark.Width = 126;
            // 
            // led
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1348, 721);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.重置);
            this.Controls.Add(this.查询);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.删除);
            this.Controls.Add(this.编辑);
            this.Controls.Add(this.新增);
            this.Controls.Add(this.dataGridView1);
            this.Name = "led";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "LED显示屏管理";
            this.Load += new System.EventHandler(this.led_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button 新增;
        private System.Windows.Forms.Button 编辑;
        private System.Windows.Forms.Button 删除;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button 查询;
        private System.Windows.Forms.Button 重置;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn cj;
        private System.Windows.Forms.DataGridViewTextBoxColumn model;
        private System.Windows.Forms.DataGridViewTextBoxColumn ip;
        private System.Windows.Forms.DataGridViewTextBoxColumn port;
        private System.Windows.Forms.DataGridViewTextBoxColumn msg_type;
        private System.Windows.Forms.DataGridViewTextBoxColumn communicate_protocol;
        private System.Windows.Forms.DataGridViewTextBoxColumn address;
        private System.Windows.Forms.DataGridViewTextBoxColumn remark;
    }
}