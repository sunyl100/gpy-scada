﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using gpy_common;
using gpy_models;

namespace gpy_ui
{
    public partial class Form5 : Form
    {

        
        public Form5()
        {
            InitializeComponent();
        }

        private void Form5_Load(object sender, EventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            var connect = Global.Instance.sqlLiteHelper.sqliteConnection;
            var Datatable = new DataTable();
            var adp = new SQLiteDataAdapter("select * from data_record", connect);
            adp.Fill(Datatable);

            AddStudent add = new AddStudent();
            add.add();


            var connect1 = Global.Instance.sqlLiteHelper.sqliteConnection;
            var Datatable1 = new DataTable();
            var adp1 = new SQLiteDataAdapter("select * from data_record", connect);
            adp1.Fill(Datatable);
           


            //数据更新，UI暂时挂起，直到EndUpdate绘制控件，可以有效避免闪烁并大大提高加载速度
            this.listView1.BeginUpdate();
            //添加5行数据
            for (int i = 0; i < 5; i++)
            {
                // ListViewItem lvi = new ListViewItem();
                ListViewItem lvi = new ListViewItem("第1列,第" + i + "行");
                lvi.SubItems.Add("第1列,第" + i + "行");
                lvi.SubItems.Add("第2列,第" + i + "行");
                lvi.SubItems.Add("第3列,第" + i + "行");
                lvi.SubItems.Add("第4列,第" + i + "行");
                lvi.SubItems.Add("第5列,第" + i + "行");
                this.listView1.Items.Add(lvi);
            }
            //结束数据处理，UI界面一次性绘制。
            this.listView1.EndUpdate();

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            AddStudent testDb = new AddStudent();
            testDb.add();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.listView1.BeginUpdate();

            var connect = Global.Instance.sqlLiteHelper.sqliteConnection;
            var Datatable = new DataTable();
            var adp = new SQLiteDataAdapter("select * from data_record", connect);
            adp.Fill(Datatable);


           

            ListStudent listStudent = new ListStudent();
           var list = listStudent.List();
            //添加5行数据
            Console.WriteLine(list.Count);
            for (int i = 0; i < list.Count; i++)
            {
                ListViewItem lvi = new ListViewItem(list[i].name + "第" + i + "行");
                lvi.SubItems.Add(list[i].age + "第" + i + "行");
                lvi.SubItems.Add(list[i].email + "第" + i + "行");
                this.listView1.Items.Add(lvi);
            }
            //结束数据处理，UI界面一次性绘制。
            this.listView1.EndUpdate();

        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            AddStudent add = new AddStudent();
            add.add();


        }

        private void button2_Click_1(object sender, EventArgs e)
        {

            AddStudent add = new AddStudent();
            add.add();

            var connect = Global.Instance.sqlLiteHelper.sqliteConnection;
            var Datatable = new DataTable();
            var adp = new SQLiteDataAdapter("select * from data_record", connect);
            adp.Fill(Datatable);


            var connect1 = Global.Instance.sqlLiteHelper.sqliteConnection;
            var Datatable1 = new DataTable();
            var adp1 = new SQLiteDataAdapter("select * from data_record", connect);
            adp1.Fill(Datatable);

            AddStudent add1 = new AddStudent();
            add1.add();


            this.listView1.BeginUpdate();
            ListStudent listStudent = new ListStudent();
            List<Student> list = listStudent.List();
            //添加5行数据
            Console.WriteLine(list.Count);
            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine(list[i].name);

                ListViewItem lvi = new ListViewItem(list[i].name + "第" + i + "行");
                lvi.SubItems.Add(list[i].age + "第" + i + "行");
                lvi.SubItems.Add(list[i].email + "第" + i + "行");
                this.listView1.Items.Add(lvi);
            }
            
            //结束数据处理，UI界面一次性绘制。
            this.listView1.EndUpdate();
        }
    }

    
}
