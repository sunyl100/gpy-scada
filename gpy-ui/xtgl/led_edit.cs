﻿using gpy_common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gpy_ui.xtgl
{
    public partial class led_edit : Form
    {
        public led led;
        public led_edit()
        {
            InitializeComponent();
        }

        public led_edit(led led)
        {
            InitializeComponent();
            this.led = led;
        }
        public led_edit(int id, led led)
        {
            InitializeComponent();
            this.led = led;
            SQLiteDataReader reader1 = Global.Instance.sqlLiteHelper.ExecuteQuery("select *  FROM sys_led WHERE id = " + id);
            while (reader1.Read())
            {
                if (!reader1.IsDBNull(reader1.GetOrdinal("id")))
                {
                    id = reader1.GetInt32(reader1.GetOrdinal("id"));
                    textBox7.Text = id + "";
                }

                if (!reader1.IsDBNull(reader1.GetOrdinal("cj")))
                {
                    textBox1.Text = reader1.GetString(reader1.GetOrdinal("cj"));
                }
                if (!reader1.IsDBNull(reader1.GetOrdinal("model")))
                {
                    textBox2.Text = reader1.GetString(reader1.GetOrdinal("model"));
                }
                if (!reader1.IsDBNull(reader1.GetOrdinal("ip")))
                {
                    textBox3.Text = reader1.GetString(reader1.GetOrdinal("ip"));
                }

                if (!reader1.IsDBNull(reader1.GetOrdinal("port")))
                {
                    textBox4.Text = reader1.GetString(reader1.GetOrdinal("port"));
                }
                if (!reader1.IsDBNull(reader1.GetOrdinal("msg_type")))
                {
                    comboBox2.Text = reader1.GetString(reader1.GetOrdinal("msg_type"));
                }
                 if (!reader1.IsDBNull(reader1.GetOrdinal("communicate_protocol")))
                {
                    comboBox3.Text = reader1.GetString(reader1.GetOrdinal("communicate_protocol"));
                }
                 if (!reader1.IsDBNull(reader1.GetOrdinal("address")))
                {
                    textBox6.Text = reader1.GetString(reader1.GetOrdinal("address"));
                }
                 if (!reader1.IsDBNull(reader1.GetOrdinal("remark")))
                {
                    textBox5.Text = reader1.GetString(reader1.GetOrdinal("remark"));
                }
                

            }

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }
  
        private void save_Click(object sender, EventArgs e)
        {
            String id = textBox7.Text;

            var cj = textBox1.Text;
            var model = textBox2.Text;
            var ip = textBox3.Text;
            var port = textBox4.Text;
            var msg_type = comboBox2.Text;
            var communicate_protocol = comboBox3.Text;
            var address = textBox6.Text;
            var remark = textBox5.Text;
            var values = new string[] { cj, model, ip, port, msg_type, communicate_protocol, address, remark };
            var clumns = new string[] { "cj", "model", "ip", "port", "msg_type", "communicate_protocol", "address", "remark" };
            if (!Valiad.ifnull(values))
            {
                MessageBox.Show("所有数据均为必填项！");
                return;
            }

            //插入数据
            Global.Instance.sqlLiteHelper.UpdateValues("sys_led", clumns, values, "id", id, "=");
            MessageBox.Show("修改成功！");
            this.Close();
            led.search();
        }
    }
}
