﻿using gpy_common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gpy_ui.xtgl
{
    public partial class mixture_add : Form
    {
        public mixture mixture;
        public mixture_add()
        {
            InitializeComponent();
        }
         public mixture_add(mixture mixture)
        {
            InitializeComponent();
            this.mixture = mixture;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void mixture_add_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var mj = textBox1.Text;
            var ph = textBox2.Text;
            var lch = textBox3.Text;
            var fg = numericUpDown2.Text;
            var hll = numericUpDown3.Text;
            var st = numericUpDown4.Text;
            var values = new string[] { mj,ph,lch,fg,hll,st };
            var clumns = new string[] { "mj", "ph", "lch", "fg", "hll", "st" };
            if (!Valiad.ifnull(values))
            {
                MessageBox.Show("所有数据均为必填项！");
                return;
            }
            //插入数据
            Global.Instance.sqlLiteHelper.InsertValuesAutoId("sys_mixture", clumns,values);
            Console.WriteLine("写入 sys_mixture" + values.ToString());
            this.Close();
            MessageBox.Show("保存成功！");
            mixture.search();

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
