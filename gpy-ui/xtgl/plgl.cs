﻿using gpy_common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace gpy_ui.xtgl
{
    public partial class plgl : Form
    {
        public plgl()
        {
            InitializeComponent();
        }


    public void search()
    {
        var Datatable = new DataTable();
        var connect = Global.Instance.sqlLiteHelper.sqliteConnection;
        var adp = new SQLiteDataAdapter("select * from sys_mixture", connect);
        adp.Fill(Datatable);
        plgllist.DataSource = Datatable;

    }

    private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            this.search();

        }

        private void plgl_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.search();
        }
    }
}
